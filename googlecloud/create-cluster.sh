#!/bin/bash

np=$1
image="ami-03d315ad33b9d49c4"
group_name="horovodcluster$RANDOM"
machine_type="e2-small"


declare -a instances_names
declare -a private_ips
declare -a public_ips

for i in $(seq 0 $((np-1)))
do
  instance_name="horovod-cluster$RANDOM"
  gcloud beta compute instances create $instance_name \
    --machine-type=$machine_type \
    --subnet=default \
    --zone=us-central1-a \
    --metadata-from-file startup-script=configure-instance.sh \
    --image=ubuntu-1804-bionic-v20210325 \
    --image-project=ubuntu-os-cloud \
    --boot-disk-size=10GB \
    --boot-disk-type=pd-balanced \
    --boot-disk-device-name=$instance_name

  private_ip=$(gcloud compute instances describe $instance_name --format="value(networkInterfaces[0].networkIP)")
  public_ip=$(gcloud compute instances describe $instance_name --format="value(networkInterfaces[0].accessConfigs[0].natIP)")
  private_ips+=($private_ip)
  public_ips+=($public_ip)
  instances_names+=($instance_name)
done


CHECK_INSTANCE_STATUS='gcloud compute instances describe $instance_name --format="value(status)"'
while [ $(eval $CHECK_INSTANCE_STATUS) != "RUNNING" ]
do
  echo "inicializando ultima instancia"
  sleep 6
done
sleep 40

echo "criando host file"
cat hosts-file-header.txt > host-file.txt
i=0
for private_ip in ${private_ips[@]}
do
  echo "$private_ip worker$i" >> host-file.txt
  i=$(($i + 1))
done

echo "distribuindo host file"
for instance_name in ${instances_names[@]}
do
  gcloud compute scp id_rsa $instance_name:/home/claro_henrique/.ssh/
  gcloud compute scp host-file.txt $instance_name:/home/claro_henrique/
done

server_instance_name=${instances_names[0]}

for instance_name in ${instances_names[@]}
do
  echo "configurando $instance_name"
  if [ "$instance_name" = "$server_instance_name" ]; then
    echo "configurando servidor"
    gcloud compute ssh $instance_name  --command="
      sudo rm /etc/hosts
      sudo cp host-file.txt /etc/hosts
      mkdir cloud
      echo '/home/claro_henrique/cloud *(rw,sync,no_root_squash,no_subtree_check)' | sudo tee /etc/exports
      sudo exportfs -a
      sudo service nfs-kernel-server restart
      cd /home/claro_henrique/cloud
      git clone https://claro_henrique@bitbucket.org/claro_henrique/horovod-tests.git"
    sleep 10
  else
    echo "configurando cliente"
    gcloud compute ssh $instance_name  --command="
      sudo rm /etc/hosts
      sudo cp host-file.txt /etc/hosts
      mkdir cloud
      sudo mount -t nfs worker0:/home/claro_henrique/cloud ~/cloud"
    sleep 4
  fi
done

echo "SERVER PUBLIC IP $public_ip_server"
echo "ssh -i $key_file ubuntu@$public_ip_server"























