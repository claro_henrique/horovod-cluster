#!/bin/bash
echo "0" >> /home/ubuntu/degub.txt
apt update

echo "1" >> /home/ubuntu/degub.txt
apt install openssh-server -y
echo "2" >> /home/ubuntu/degub.txt
apt install nfs-kernel-server -y
echo "3" >> /home/ubuntu/degub.txt
apt install nfs-common -y

echo "4" >> /home/ubuntu/degub.txt
apt install python3-dev python3-pip python3-venv -y
echo "5" >> /home/ubuntu/degub.txt

pip3 install --upgrade pip
echo "6" >> /home/ubuntu/degub.txt
pip3 install --upgrade tensorflow
echo "7" >> /home/ubuntu/degub.txt
pip3 install keras
echo "8" >> /home/ubuntu/degub.txt
pip3 install torch==1.8.1+cpu torchvision==0.9.1+cpu torchaudio==0.8.1 -f https://download.pytorch.org/whl/torch_stable.html
#sudo apt install mpich -y
apt install cmake -y
echo "9" >> /home/ubuntu/degub.txt
HOROVOD_WITH_GLOO=1
pip3 install horovod[tensorflow,keras,pytorch]
echo "10" >> /home/ubuntu/degub.txt





