#!/bin/bash
key=$1
key_file=$2
np=$3
image="ami-0b70285e5215b80eb"
group_name="horovodcluster$RANDOM"
instance_type=$4

set -x
echo "configurando grupo de seguranca"
vpc_id=$(aws ec2 describe-vpcs --filters Name=is-default,Values=true --query "Vpcs[0].VpcId" --output text)
subnet_id=$(aws ec2 describe-subnets --filters "Name=vpc-id,Values=$vpc_id" --query "Subnets[0].SubnetId" --output text)
sgid=$(aws ec2 create-security-group --group-name $group_name --description "Horovod Cluster" --output text) #--vpc-id $VPC_ID

aws ec2 authorize-security-group-ingress --group-id $sgid --protocol tcp --port 22 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $sgid --protocol tcp --port 0-65534 --source-group $sgid 

declare -a private_ips
declare -a public_ips


for i in $(seq 0 $((np-1)))
do
  #--placement 'GroupName=horovod-placement-group'
  #--placement 'AvailabilityZone=us-east-1a,GroupName=horovod-placement-group' \
  #--user-data file://configure-gpu-instance.sh \
  #--block-device-mappings '[{ "DeviceName": "/dev/sda1", "Ebs": { "VolumeSize": 100 } }]' \
  instance_id=$(aws ec2 run-instances \
    --image-id $image \
    --instance-type $instance_type \
    --key-name $key \
    --placement 'AvailabilityZone=us-east-1a,GroupName=horovod-placement-group' \
    --security-group-ids $sgid \
    --user-data file://configure-instance.sh \
    --query Instances[0].InstanceId --output text)
  private_ip=$(aws ec2 describe-instances --instance-ids $instance_id --query "Reservations[0].Instances[0].PrivateIpAddress" --output text)
  public_ip=$(aws ec2 describe-instances --instance-ids $instance_id --query "Reservations[0].Instances[0].PublicIpAddress" --output text)
  private_ips+=($private_ip)
  public_ips+=($public_ip)
done


CHECK_INSTANCE_STATUS='aws ec2 describe-instance-status --instance-ids $instance_id --query "InstanceStatuses[0].InstanceState.Name" --output text'
while [ $(eval $CHECK_INSTANCE_STATUS) != "running" ]
do
  echo "inicializando ultima instancia"
  sleep 6
done
sleep 120


cat hosts-file-header.txt > host-file.txt
i=0
for private_ip in ${private_ips[@]}
do
  echo "add to hostfile: $private_ip"
  echo "$private_ip worker$i" >> host-file.txt
  i=$(($i + 1))
done

for public_ip in ${public_ips[@]}
do
  echo "ubuntu@""$public_ip":/home/ubuntu/
  scp -o "StrictHostKeyChecking no" -i $key_file host-file.txt $key_file "ubuntu@""$public_ip":/home/ubuntu/
  ssh -o "StrictHostKeyChecking no" -i $key_file ubuntu@"$public_ip" -t "sudo rm /etc/hosts ; sudo cp host-file.txt /etc/hosts ; cp $key_file .ssh/id_rsa"
done

public_ip_server=${public_ips[0]}

for public_ip in ${public_ips[@]}
do
  if [ "$public_ip" = "$public_ip_server" ]; then
    ssh -i $key_file ubuntu@"$public_ip" -t "
      mkdir /home/ubuntu/cloud
      echo '/home/ubuntu/cloud *(rw,sync,no_root_squash,no_subtree_check)' | sudo tee /etc/exports
      sudo exportfs -a
      sudo service nfs-kernel-server restart
      cd /home/ubuntu/cloud
      git clone https://claro_henrique@bitbucket.org/claro_henrique/horovod-tests.git"
    sleep 10
  else
    ssh -i $key_file ubuntu@"$public_ip" -t "
      mkdir /home/ubuntu/cloud
      sudo mount -t nfs worker0:/home/ubuntu/cloud ~/cloud"
    sleep 1
  fi
done

echo "SERVER PUBLIC IP $public_ip_server"
echo "ssh -i $key_file ubuntu@$public_ip_server"























